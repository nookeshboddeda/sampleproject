const express = require('express');
const mongoose = require('mongoose');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('express-jwt');
var secret = "!@#DWe$%^gge&&**";
const registerController = require('./controllers/registrationController');
const loginController = require('./controllers/loginController');
// setInterval(()=>{
// },30000)

mongoose.connect('mongodb://localhost/my_database', {
    useNewUrlParser: true,
    useUnifiedTopology: true
},(err,success)=>{
    console.log("connection successfully created");
});


app.use('/api', jwt({ secret: secret }).unless({ path: ['/register', '/login'] }));
app.use(cors());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json());
app.use('/register', registerController);
app.use('/login', loginController);
app.use('/api',require('./controllers/worddefn.controller'))
app.listen(5000, () => {
    console.log('server connected successfully')
})