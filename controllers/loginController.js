const express = require('express');
var router = express.Router();
const loginService = require('./../services/loginService');
router.post('/', login);

function login(req, res) {
  
    let body = req.body;
    loginService.login(body).then(success => {
        res.send(success);
    }).catch(err => {
        res.send(err);
    })

}

module.exports = router;