const express = require('express');
var router = express.Router();
const mockJson = require('./../mocks/mock.json').words;
router.get('/word/:word/definitions', wordDefn);
router.get('/word/:word/synonyms',synonyms);
router.get('/word/:word/antonyms',antonyms);
router.get('/word/:word/examples',examples);
router.get('/word/:word/dict',dictionary);
router.get('/words/randomWord',fullDictionary);






function wordDefn(req, res) {
    let searchWord = req.params.word;
    let resultArray = [];
    mockJson.forEach(d => {
        if (d.word == searchWord) {
            d.results.forEach(r => {
                if (r.hasOwnProperty("definition")) {
                    resultArray.push(r.definition);
                }
            })
            console.log("==================>>def",resultArray)
          
        }
    })
    if(resultArray.length == 0){
        res.send({msg:'definitions not available for your searched key'})
    }else{
        res.send({definitions:resultArray})
    }
}

function synonyms(req,res){
    let searchWord = req.params.word;
    let resultArray = [];
    mockJson.forEach(d => {
        if (d.word == searchWord) {
            d.results.forEach(r => {
                if (r.hasOwnProperty("synonyms")) {
                    resultArray.push(r.synonyms);
                }
            })
            // console.log("==================>>",resultArray.flat())
          
        }
    })
    if(resultArray.length == 0){
        res.send({msg:'synonyms not available for your searched key'})
    }else{
        res.send({synonyms:resultArray.flat()})
    }
}

function antonyms(req,res){
    console.log("==============antonyms")
    let searchWord = req.params.word;
    let resultArray = [];
    mockJson.forEach(d => {
        if (d.word == searchWord) {
            d.results.forEach(r => {
                if (r.hasOwnProperty("antonyms")) {
                    resultArray.push(r.antonyms);
                }
            })
            console.log("==================>>",resultArray.flat())
          
        }
    })
    if(resultArray.length == 0){
        res.send({msg:'antonyms not available for your searched key'})
    }else{
        res.send({antonyms:resultArray.flat()})
    }
}

function examples(req,res){
    console.log("==============examples")
    let searchWord = req.params.word;
    let resultArray = [];
    mockJson.forEach(d => {
        if (d.word == searchWord) {
            d.results.forEach(r => {
                if (r.hasOwnProperty("examples")) {
                    resultArray.push(r.examples);
                }
            })
            console.log("==================>>",resultArray.flat())
          
        }
    })
    if(resultArray.length == 0){
        res.send({msg:'examples not available for your searched key'})
    }else{
        res.send({examples:resultArray.flat()})
    }
}

function dictionary(req,res){
    let searchWord = req.params.word;
    let resultArray = [];
    mockJson.forEach(d => {
        if (d.word == searchWord) {
            d.results.forEach(r => {
                if (r.hasOwnProperty("examples") || r.hasOwnProperty("synonyms") || r.hasOwnProperty("antonyms") || r.hasOwnProperty("definition") ) {
                    resultArray.push({examples:r.examples?r.examples:'',synonyms:r.synonyms,antonyms:r.antonyms,definitions:r.definition});
                }
            })
          
        }
    })
    if(resultArray.length == 0){
        res.send({msg:'examples not available for your searched key'})
    }else{
        res.send({examples:resultArray})
    }
}

function fullDictionary(req,res){
    let mockArray = mockJson.map(d=>{
        return d.word;
    })
    let searchWord = mockArray[Math.floor(Math.random() * mockArray.length)];
    let resultArray = [];
    mockJson.forEach(d => {
        if (d.word == searchWord) {
            d.results.forEach(r => {
                if (r.hasOwnProperty("examples") || r.hasOwnProperty("synonyms") || r.hasOwnProperty("antonyms") || r.hasOwnProperty("definition") ) {
                    resultArray.push({examples:r.examples?r.examples:'',synonyms:r.synonyms,antonyms:r.antonyms,definitions:r.definition});
                }
            })
          
        }
    })
    if(resultArray.length == 0){
        res.send({msg:'examples not available for your searched key'})
    }else{
        res.send({searchWord,examples:resultArray})
    }
}



module.exports = router;