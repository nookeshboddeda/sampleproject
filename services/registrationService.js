const Q = require('q');
const Schema = require('./../schemas/userSchemas');
const Bcrypt = require('bcryptjs')


register = (credentials) => {
  let defer = Q.defer();

 credentials.password = Bcrypt.hashSync(credentials.password, 10);
 let favourite = new Schema(credentials);
  favourite.save().then((results, err) => {
      if (err) throw err;
      defer.resolve({ error: '',data:results, success: true })
  }).catch((err) => {
      defer.reject({ error: err, success: false });
  })


  return defer.promise;
}

module.exports = {register};