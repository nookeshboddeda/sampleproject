const Q = require('q');
const Schema = require('./../schemas/userSchemas');
const Bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');
var secret="!@#DWe$%^gge&&**";


login = (credentials) => {
    let defer = Q.defer();
    Schema.findOne({email:credentials.email},(err,data)=>{
        console.log('credential for login',data)
        if(data == null){
            defer.reject({msg:'user not registered'})
        }else{
            if(data){
                if(!Bcrypt.compareSync(credentials.password,data.password)){
                    defer.reject({error:true,msg:'invalid password'})
                }else{
                    delete data.password;
                    let token = jwt.sign({sub:data._id},secret,{
                        expiresIn: 86400 // expires in 24 hours
                      });
                    defer.resolve({token:token,data:data})
                }
            }
        }
    })
  
  
    return defer.promise;
  }
  
module.exports = {login};